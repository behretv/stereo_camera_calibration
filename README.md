# stereo_camera_calibration

Recording images and calibrate a stereo camera rig via MATLAB. Alle events are
handled by starting the main functions and can be chosen by user input. Before 
the images can be recorded a folder "images" is created by "RecordImagePairs.m",
which contains a "left" and a "right" folder. The function to record the images is 
chosing these folders to save the recorded images.

To start the program run:
`main`