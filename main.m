clear
close all

%% Set-up
addpath('src/')
path = fullfile(pwd,'images');

%% Questions
q1 = input('Do you want to record images? (0/1)');

% Set-up webcams
w = webcamlist;
idx = find(contains(w,'c922 Pro Stream Webcam'));
cam1 = webcam(idx(1));
cam2 = webcam(idx(2));

if q1
    avblRes1 = cam1.AvailableResolutions;
    avblRes2 = cam2.AvailableResolutions;
    disp('Availbele resolutions:')
    disp([avblRes1',avblRes2'])
    idx = input('Choose the index of the resolution you want to use?');
    cam1.Resolution = avblRes1{idx};
    cam2.Resolution = avblRes2{idx};
else
    
end

q2 = input('Do you want to calibrate? (0/1)');

%% Outline

% #1 Record images
if q1 
    nImages = 30;
    imgData = RecordImagePairs(cam1,cam2,path);
    imgData.Start(nImages);
    disp(imgData.cam1);
    disp(imgData.cam2);
end

% #2 Get cam resolution from images
try
    I = imread(fullfile('images','left','1.png'));
    sz = size(I);
    resStr = [num2str(sz(2)),'x',num2str(sz(1))];
    cam1.Resolution = resStr;
    cam2.Resolution = resStr;
catch
    disp('There are no images for the calibration!')
end

% #3 Calibration
if q2
    squareSize = 48;
    calibrationData = StereoCalibration(path,squareSize);
    stereoParams = calibrationData.GetParameter();
    save('stereoParams.mat','stereoParams');
else
    load('stereoParams.mat');
end

% #4 3D Recontruction
try
    stereoStream = StereoVideoStream(cam1,cam2,stereoParams);
catch
    disp('No calibration parameters?')
end

% Clear cam objects at the end
clear cam1
clear cam2
