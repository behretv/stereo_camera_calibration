classdef RecordImagePairs    
    %STEREOCALIBRATION Recording images for stereo calibration
    %   At first the webcamsd are detected, their name is spcefied in the
    %   camNames attribute. Then the SaveImage-method is executed which
    %   takes snapshots and saves the image at the end.   
    properties
        %% Eigenschaften der Klasse
        cam1;
        cam2;       
    end
    properties (Constant)    
             camNames = 'C922 Pro Stream Webcam';
    end
    properties (Access = private)
        path;
    end
    methods (Access = public)        
        function obj = RecordImagePairs(cam1,cam2,imgPath)
            %% Constructor
            obj.path = imgPath;
            obj.cam1 = cam1;
            obj.cam2 = cam2;
            
            if (exist(imgPath,'dir') ~= 7)
                mkdir(imgPath)
            end
        end
        function obj = Start(obj,n)
            % Sound: Start
            obj.PlaySinus(0.3, 400,5);
            
            % Saves the images for the calibration
            obj.SaveImages(n);
        end
    end
    methods (Access = private)
        function SaveImages(obj,nImages)
            %% Saving n images per cam
            path1 = fullfile(obj.path,'left');
            path2 = fullfile(obj.path,'right');
            if (exist(path1,'dir') ~= 7)
                mkdir(path1)
            end
            if (exist(path2,'dir') ~= 7)
                mkdir(path2)
            end
            
            [r1,r2] = obj.getResolution(obj.cam1);
            A = uint8(zeros(r2,r1, 3));
            img12 = imshow([A,A]);

            img1 = cell(nImages,1);
            img2 = cell(nImages,1);
            
            for nx = 1:nImages
                % Creats empty images
                img1{nx} = uint8(zeros(r2,r1,3));
                img2{nx} = uint8(zeros(r2,r1,3));
            end
            
            for n = 1 : nImages
                % Recors the images
                obj.PlaySinus(0.3, 200,3);
                
                img1{n} = snapshot(obj.cam1);
                img2{n} = snapshot(obj.cam2);
                
                set(img12,'cdata',[img1{n}, img2{n}]);
                pause(2)
            end
            
            for nSave = 1:nImages
                % Saves the images
                imwrite(img1{nSave}, fullfile(path1, sprintf('%d.png', nSave)));
                imwrite(img2{nSave}, fullfile(path2, sprintf('%d.png', nSave)));
            end
        end
    end
    methods (Static)
        function [r1,r2] = getResolution(cam)
            %% Get image resolution
            r = cam.Resolution;
            c = strsplit(r,'x');
            r1 = str2double(c{1});
            r2 = str2double(c{2});
        end
        function PlaySinus(PlayTime, Fc,j)
            %% Time specifications:
            Fs = 48000;                 % samples per second
            dt = 1/Fs;                  % seconds per sample
            t = (0:dt:PlayTime-dt)';    % seconds

            for i = 1:1:j
                y = cos(2*pi*Fc*t*i);     % Generate sine wave   
                sound(y,Fs);            % Play sound
                if j ~= 1
                    pause(PlayTime)     % Pause
                end
            end            
        end
    end    
end
