classdef StereoCalibration
    %STEREOCALIBRATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private, GetAccess = public)
        imageSize;
        imagePoints;
        imagesUsed;
        worldPoints;
        parameters;
    end
    
    methods (Access = public)
        function obj = StereoCalibration(path,squareSize)
            %STEREOCALIBRATION Construct an instance of this class
            %   Detailed explanation goes here
            disp('Stereo calibration:')
            leftImages = imageDatastore(fullfile(path,'left'));
            rightImages = imageDatastore(fullfile(path,'right'));
            
            disp('-Generate data from images for calibration')
            obj = obj.GenerateDataFromImages(leftImages,rightImages,squareSize);
            
            disp('-Calibarte')
            obj = obj.Calibrate();
            
            if ~verLessThan('matlab','9.0')
                disp('-Recalibarte')
                obj = obj.Recalibrate();
            end
        end
        function params = GetParameter(obj)
            params = obj.parameters;
        end
    end
    methods(Access = private)
        function obj = GenerateDataFromImages(obj,leftI,rightI,squareSize)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            [XI,boardSize,idx] =  detectCheckerboardPoints(leftI.Files,rightI.Files);
            XW = generateCheckerboardPoints(boardSize,squareSize);
            I = readimage(leftI,1); 
            sz = size(I);
            
            % Save temporary results in object
            obj.imagePoints = XI;
            obj.worldPoints = XW;
            obj.imagesUsed = idx;
            obj.imageSize = sz(1:2);
        end
    
        function obj = DeleteInvalidImages(obj,imageFileNames)
            % Deleting images if the pattern could not be found
            invalidImgs = imageFileNames(~obj.imagesUsed);
            disp('Deleting:')
            for j = 1:size(invalidImgs,1)
                disp(invalidImgs(j))
                delete( invalidImgs{j} );
            end
        end
        function obj = Calibrate(obj)
            % Inita calibration
            params = estimateCameraParameters(obj.imagePoints,...
                                    obj.worldPoints,...
                                    'NumRadialDistortionCoefficients',3,...
                                    'EstimateTangentialDistortion',true);
            e1 = mean(mean(abs(params.CameraParameters1.ReprojectionErrors)));
            e2 = mean(mean(abs(params.CameraParameters2.ReprojectionErrors)));
            fprintf('Inital calibration MRE: %4.3f\n',mean([e1(:);e2(:)]));
            
            % Set results
            obj.parameters = params;
        end
        function obj = Recalibrate(obj)
            % Remove outliers
            e1 = mean(mean(abs(obj.parameters.CameraParameters1.ReprojectionErrors)));
            e2 = mean(mean(abs(obj.parameters.CameraParameters2.ReprojectionErrors)));
            TF1 = isoutlier(e1(:),'mean');
            TF2 = isoutlier(e2(:),'mean');
            TF = any([TF1,TF2],2);
            obj.imagePoints = obj.imagePoints(:,:,~TF,:);
            fprintf('Removing %.0d outliers\n',sum(TF))
            
            % Re-fined calibration
            params = estimateCameraParameters(obj.imagePoints,...
                                    obj.worldPoints,...
                                    'NumRadialDistortionCoefficients',3,...
                                    'EstimateTangentialDistortion',true,...
                                    'ImageSize', obj.imageSize);
            e1 = mean(mean(abs(params.CameraParameters1.ReprojectionErrors)));
            e2 = mean(mean(abs(params.CameraParameters2.ReprojectionErrors)));
            fprintf('Re-calibration MRE: %4.3f\n',mean([e1(:);e2(:)]));
            
            % Set results
            obj.parameters = params;
        end
    end
end

