classdef StereoVideoStream
    %STEREOVIDEOSTREAM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private)
        disparityRange = 16*[0 10];
        blockSize = 5;
        threshold = 0.05;
        ax1;
        ax2;
        ax3;
        ax4;
        showVideo = true;
        showRecti = true;
        showDispa = true;
        showPoint = true;
    end
    properties(Constant)
        imgScale = 0.5;
         k = 200;              % Number of significant features
        v = [1200 04 07 0.48]; % Parameters for calibration check
    end
    methods(Access = public)
        function obj = StereoVideoStream(cam1,cam2,stereoParams)
            %STEREOVIDEOSTREAM Construct an instance of this class
            %   Detailed explanation goes here
            resStr = strrep(cam1.Resolution,'x',' ');
            res = str2num(resStr);
            
            obj = obj.InitWindows();
            
            % Show current image
            Il = snapshot(cam1);
            Ir = snapshot(cam2);
            
            if obj.showVideo
                image(obj.ax1,[Il,Ir])
            end

            % #1 Rectification
            [Rl, Rr] = rectifyStereoImages(Il, Ir, stereoParams);
            obj = obj.CheckRectification(Rl,Rr);

            % #2 Disparity
            Jl  = rgb2gray(Rl);
            Jr = rgb2gray(Rr);
            disparityMap = disparity(Jl,Jr,'BlockSize',obj.blockSize,...
                                    'DisparityRange',obj.disparityRange);
            
            if obj.showDispa
                imshow(disparityMap, obj.disparityRange, 'Parent', obj.ax3);
                title('Disparity Map');
                colormap jet
                colorbar
            end

            
            %% Loop
            run = 1;
            while run < 5
                % Read the frames.
                tmpIr = snapshot(cam1);
                tmpIl = snapshot(cam2);

                % Rectify the frames.
                [tmpRl, tmpRr] = rectifyStereoImages(tmpIl, tmpIr, stereoParams);

                % Convert to grayscale.
                Jl  = rgb2gray(tmpRl);
                Jr = rgb2gray(tmpRr);

                % Compute disparity. 
                disparityMap = disparity(Jl, Jr,'BlockSize',obj.blockSize+run*2,...
                                        'TextureThreshold',obj.threshold,...
                                        'DisparityRange',obj.disparityRange);

                % show corrected disparity
                if obj.showDispa
                    imshow(disparityMap, obj.disparityRange, 'Parent', obj.ax3);
                end
                
                run = run+1;
                fprintf('\n%d# iteration',run);
                pause(0.1)
            end
            
            % Show Original Images
            if obj.showVideo
                image(obj.ax1,[Il,Ir])
            end

            % Reconstruct 3-D scene.
            points3D = reconstructScene(disparityMap, stereoParams);
            points3D = points3D ./ 1000;
            ptCloud = pointCloud(points3D, 'Color', Rl);

            % Create a streaming point cloud viewer
            if obj.showPoint
                pcshow(ptCloud,'Parent',obj.ax4)
            end
        end
    end
    methods (Access = private)
        function obj = InitWindows(obj)
            if obj.showVideo
                fig1 = figure('Name','Cam1 & Cam2','NumberTitle','off'); 
                movegui(fig1,'northwest');
                obj.ax1 = axes(fig1);
            end
            if obj.showRecti
                fig2 = figure('Name','Rectification','NumberTitle','off'); 
                movegui(fig2,'northeast');
                obj.ax2 = axes(fig2);
                title(obj.ax2,'Original images and matching feature points');
            end
            if obj.showDispa
                fig3 = figure('Name','Disparity Map','NumberTitle','off');
                movegui(fig3,'southeast');
                obj.ax3 = axes(fig3);
            end
            if obj.showPoint
                fig4 = figure('Name','Point Cloud','NumberTitle','off');
                movegui(fig4,'southwest');
                obj.ax4 = axes(fig4);
            end
        end
        function obj = CheckRectification(obj,Rl,Rr)
            % Convert Images
            I1gray = rgb2gray(Rl);
            I2gray = rgb2gray(Rr);

            % Detect SURF features
            blobs1 = detectSURFFeatures(I1gray, 'MetricThreshold', obj.v(1),'NumScaleLevels',obj.v(2));
            blobs2 = detectSURFFeatures(I2gray, 'MetricThreshold', obj.v(1),'NumScaleLevels',obj.v(2));

            % Seltect strongest k
            blobs1 = selectStrongest(blobs1, obj.k);
            blobs2 = selectStrongest(blobs2, obj.k);

            % Extract features
            [features1, validBlobs1] = extractFeatures(I1gray, blobs1,'FeatureSize',128);
            [features2, validBlobs2] = extractFeatures(I2gray, blobs2,'FeatureSize',128);

            % Match features.
            indexPairs = matchFeatures(features1, features2,...
                                        'Metric', 'SAD', ...
                                        'MatchThreshold', obj.v(3),...
                                        'Method','Approximate',...
                                        'MaxRatio',obj.v(4),...
                                        'Unique' ,true);

            % Retrieve locations of matched points for each image.
            m1 = validBlobs1(indexPairs(:,1),:);
            m2 = validBlobs2(indexPairs(:,2),:);
            
            % Differences of the locations
            tempdiff = m1.Location-m2.Location;
            x = abs(tempdiff(:,1));
            y = abs(tempdiff(:,2));            
            fprintf('Mean difference y: %3.1f\n',mean(y));
            
            d1 = floor(min(x)/16-1)*16;
            d2 = ceil(max(x)/16+1)*16;
            obj.disparityRange = [d1 d2];
            fprintf('Set disparity range: [%3.1f, %3.1f]\n',obj.disparityRange);
            
            if obj.showRecti
                showMatchedFeatures(Rl, Rr,m1,m2,'montage','Parent',obj.ax2);
            end
        end
    end
end

